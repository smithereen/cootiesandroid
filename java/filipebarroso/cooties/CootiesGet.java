package filipebarroso.cooties;

import android.location.Location;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by filipebarroso on 25/06/14.
 */
public class CootiesGet
{
    static class CootiesFound
    {
        Loc loc;
    }

    static class Loc
    {
        List<Double> coordinates;
    }

    interface CootiesClient
    {
        @Headers("Content-Type: application/json")
        @POST("/find")
        void getList(@Body CootiesFoundContract request, Callback<List<CootiesFound>> callback);
    }

    public void execute(CootiesFoundContract request, CootiesGetCallback callback)
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(CootiesConstants.Server.WEB_API_URL)
                .build();
        CootiesClient chatsClient = restAdapter.create(CootiesClient.class);
        chatsClient.getList(request, callback);
    }

    public static abstract class CootiesGetCallback implements Callback<List<CootiesFound>>
    {
        public abstract void success(List<CootiesFound> chats);

        public abstract void failure(String error);

        @Override
        public void failure(RetrofitError error) {
            failure(error.getMessage());
        }

        @Override
        public void success(List<CootiesFound> responseData, Response response) {
            success(responseData);
        }
    }

    public static class CootiesFoundContract
    {
        public double lat;
        public double lng;

        public CootiesFoundContract(Location location)
        {
            if(location != null)
            {
                lng = location.getLongitude();
                lat = location.getLatitude();
            }
        }
    }
}
