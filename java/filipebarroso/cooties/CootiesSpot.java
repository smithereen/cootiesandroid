package filipebarroso.cooties;

import android.location.Location;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;

/**
 * Created by filipebarroso on 25/06/14.
 */
public class CootiesSpot
{
    interface ChatsClient
    {
        @Headers("Content-Type: application/json")
        @POST("/add")
        void sendChat(@Body CootiesSpotRequest chat, Callback<String> callback);
    }

    public void execute(CootiesSpotRequest cootie, CootiesSpotCallback callback)
    {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(CootiesConstants.Server.WEB_API_URL)
                .build();
        ChatsClient chatsClient = restAdapter.create(ChatsClient.class);
        chatsClient.sendChat(cootie, callback);

    }

    public static abstract class CootiesSpotCallback implements Callback<String>
    {
        public abstract void success(int response);
        public abstract void failure(String error);

        @Override
        public void failure(RetrofitError error)
        {
            failure("");
        }
        @Override
        public void success(String v, Response arg1)
        {
            success(1);
        }
    }


    public static class CootiesSpotRequest
    {
        public Loc loc;

        public CootiesSpotRequest(Location lastLocation)
        {
            this.loc = new Loc();
            this.loc.lat = lastLocation.getLatitude();
            this.loc.lng = lastLocation.getLongitude();
        }

        public class Loc
        {
            public double lat;
            public double lng;
        }
    }
}
