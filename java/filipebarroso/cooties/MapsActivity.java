package filipebarroso.cooties;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener
{
    private SupportMapFragment fragment;
    private GoogleMap _map;
    private LocationClient _locationClient;


    Context _context;
    Button _check;
    Button _refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        _context = getBaseContext();

        FragmentManager fm = getSupportFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null)
        {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }

        setUpMapIfNeeded();


        _check = (Button) findViewById(R.id.button_check);
        _refresh = (Button) findViewById(R.id.button_refresh);

        _check.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                addSpotPosition();
            }
        });

        _refresh.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                refreshMapWithPosition();

            }
        });

        _locationClient = new LocationClient(this,this,this);
    }

    private void addSpotPosition()
    {
        CootiesSpot.CootiesSpotRequest _request = new CootiesSpot.CootiesSpotRequest(_locationClient.getLastLocation());

        CootiesSpot _client = new CootiesSpot();

        _client.execute(_request, new CootiesSpot.CootiesSpotCallback()
        {
            @Override
            public void success(int response)
            {
                refreshMapWithPosition();
                Toast.makeText(_context,"You've alert others for a Cootie",Toast.LENGTH_LONG).show();
            }

            @Override
            public void failure(String error)
            {
                refreshMapWithPosition();
                Toast.makeText(_context,"You've alert others for a Cootie",Toast.LENGTH_LONG).show();
            }
        });

    }

    private void refreshMapWithPosition()
    {
        CootiesGet.CootiesFoundContract _request = new CootiesGet.CootiesFoundContract(_locationClient.getLastLocation());
        CootiesGet _client = new CootiesGet();
        _client.execute(_request, new CootiesGet.CootiesGetCallback()
        {
            @Override
            public void success(List<CootiesGet.CootiesFound> cooties)
            {
                if(cooties == null)
                    return;

                _map.clear();
                for(CootiesGet.CootiesFound cootie : cooties)
                {
                    _map.addMarker(
                            new MarkerOptions()
                                    .position(new LatLng(cootie.loc.coordinates.get(1), cootie.loc.coordinates.get(0)))
                                    .alpha(0.5f)
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                    );
                }

                Location current = _locationClient.getLastLocation();
                LatLng myLatLng = new LatLng(current.getLatitude(), current.getLongitude());
                _map.addMarker(
                        new MarkerOptions()
                                .position(myLatLng)
                                .title("MyLocation")
                                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW))
                );

                Toast.makeText(_context,"Map refreshed",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(String error) {

            }
        });
    }


    private void setUpMapIfNeeded()
    {
        if (_map == null)
        {
            _map = fragment.getMap();
            if (_map != null)
            {
                setUpMap();
            }
        }
    }

    private void setUpMap()
    {
        if(_locationClient != null && _locationClient.isConnected())
        {
            Location current = _locationClient.getLastLocation();
            LatLng myLatLng = new LatLng(current.getLatitude(), current.getLongitude());
            _map.addMarker(
                    new MarkerOptions()
                            .position(myLatLng)
                            .title("MyLocation")
                            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN))
            );

            CameraUpdate camera = CameraUpdateFactory.newLatLngZoom(myLatLng, 15);
            _map.animateCamera(camera);
        }
    }

    @Override
    public void onConnected(Bundle bundle)
    {
        setUpMapIfNeeded();
    }

    @Override
    public void onDisconnected()
    {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {

    }

    @Override
    protected void onResume()
    {
        super.onResume();
        connectLocationClient();
    }

    @Override
    public void onStart()
    {
        super.onStart();
        connectLocationClient();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        disconnectLocationClient();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        disconnectLocationClient();
    }

    private void connectLocationClient()
    {
        if(_locationClient != null && !_locationClient.isConnected())
        {
            _locationClient.connect();
        }
    }

    private void disconnectLocationClient()
    {
        if(_locationClient != null && _locationClient.isConnected())
        {
            _locationClient.disconnect();
        }
    }

    public Location getCurrentLocation()
    {
        if(_locationClient != null && _locationClient.isConnected())
            return _locationClient.getLastLocation();
        else
            return null;
    }
}
